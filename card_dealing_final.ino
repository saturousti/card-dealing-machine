#include <Servo.h>
#include <ezButton.h>

int motorin2 = 2;
int motorin1 = 3;
int kytkin = 5;
ezButton button(13);
Servo servo_10;
bool loopison = false;

void setup()
{
  Serial.begin(9600);
  pinMode(LED_BUILTIN, OUTPUT);
  //pinMode(button, INPUT);
  pinMode(motorin2, OUTPUT);
  pinMode(motorin1, OUTPUT);
  pinMode(kytkin, OUTPUT);
  servo_10.attach(10, 0, 180);
  button.setDebounceTime(50);
}
void loop() {
button.loop();
if(loopison){
  if(button.isPressed()){
    loopison = false;
    Serial.println("off");
    } 
}
if (!loopison){
 if(button.isPressed()){
   loopison = true;
   Serial.println("on");
 }
}
if (loopison){
    delay(2000);
    
    analogWrite(kytkin, 155);
    servo_10.write(0);
    digitalWrite(motorin1, LOW);
    digitalWrite(motorin2, HIGH); // motor dispenses card
    delay(500);
    digitalWrite(motorin2, LOW); // HERE motor turns the other way so no more cards get dealt
    digitalWrite(motorin1, HIGH);
    //analogWrite(kytkin, 255);
    delay(300);
    digitalWrite(motorin1, LOW);
    servo_10.write(45);
    delay(500);

    //analogWrite(kytkin, 200);
    digitalWrite(motorin1, LOW);
    digitalWrite(motorin2, HIGH); // motor dispenses card
    delay(500);
    digitalWrite(motorin2, LOW); // HERE motor turns the other way so no more cards get dealt
    digitalWrite(motorin1, HIGH);
    //analogWrite(kytkin, 255);
    delay(300);
    digitalWrite(motorin1, LOW);
    servo_10.write(90);
    delay(500);

    //analogWrite(kytkin, 200);
    digitalWrite(motorin1, LOW);
    digitalWrite(motorin2, HIGH); // motor dispenses card
    delay(500);
    digitalWrite(motorin2, LOW); // HERE motor turns the other way so no more cards get dealt
    digitalWrite(motorin1, HIGH);
    //analogWrite(kytkin, 255);
    delay(300);
    digitalWrite(motorin1, LOW);
    servo_10.write(135);
    delay(500);
 
    //analogWrite(kytkin, 200);
    digitalWrite(motorin1, LOW);
    digitalWrite(motorin2, HIGH); // motor dispenses card
    delay(500);
    digitalWrite(motorin2, LOW); // HERE motor turns the other way so no more cards get dealt
    digitalWrite(motorin1, HIGH);
    //analogWrite(kytkin, 255);
    delay(300);
    digitalWrite(motorin1, LOW);
    servo_10.write(180);
    delay(500);

    //analogWrite(kytkin, 200);
    digitalWrite(motorin1, LOW);
    digitalWrite(motorin2, HIGH); // motor dispenses card
    delay(500);
    digitalWrite(motorin2, LOW); // HERE motor turns the other way so no more cards get dealt
    digitalWrite(motorin1, HIGH);
    //analogWrite(kytkin, 255);
    delay(300);
    digitalWrite(motorin1, LOW);
    servo_10.write(0);
    delay(500);
    
}
}